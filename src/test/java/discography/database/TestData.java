package discography.database;

import org.hibernate.Session;

import discography.model.Album;
import discography.model.Artist;
import discography.model.Track;
import discography.util.HibernateUtil;

public class TestData {
	public static void main(String[] args){
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		
		Artist artist1 = new Artist();
		Artist artist2 = new Artist();
		artist1.setName("Queen");
		artist2.setName("Westlife");	
		
		session.save(artist1);
		session.save(artist2);
		
		Album album1_1 = new Album();
		Album album2_1 = new Album();
		Album album2_2 = new Album();
		album1_1.setName("Innuendo");
		album1_1.setYear(1991);
		album1_1.setArtist(artist1);
		album2_1.setName("Westlife");
		album2_1.setYear(1999);
		album2_1.setArtist(artist2);
		album2_2.setName("Coast to Coast");
		album2_2.setYear(2000);
		album2_2.setArtist(artist2);
		artist1.getAlbums().add(album1_1);
		artist2.getAlbums().add(album2_1);
		artist2.getAlbums().add(album2_2);
		
		session.save(album1_1);
		session.save(album2_1);
		session.save(album2_2);
		
		Track track1_1_1 = new Track();
		Track track1_1_2 = new Track();
		Track track1_1_3 = new Track();
		Track track2_1_1 = new Track();
		Track track2_1_2 = new Track();
		Track track2_2_1 = new Track();
		track1_1_1.setName("I'm Going Slightly Mad");
		track1_1_1.setAlbum(album1_1);
		track1_1_1.setArtist(artist1);
		track1_1_2.setName("Headlong");
		track1_1_2.setAlbum(album1_1);
		track1_1_2.setArtist(artist1);
		track1_1_3.setName("I Can't Live With You");
		track1_1_3.setAlbum(album1_1);
		track1_1_3.setArtist(artist1);
		track2_1_1.setName("My love");
		track2_1_1.setAlbum(album2_1);
		track2_1_1.setArtist(artist2);
		track2_1_2.setName("Fly without wings");
		track2_1_2.setAlbum(album2_1);
		track2_1_2.setArtist(artist2);
		track2_2_1.setName("Seasons in the sun");
		track2_2_1.setAlbum(album2_2);
		track2_2_1.setArtist(artist2);
		album1_1.getTracks().add(track1_1_1);
		album1_1.getTracks().add(track1_1_2);
		album1_1.getTracks().add(track1_1_3);
		album2_1.getTracks().add(track2_1_1);
		album2_1.getTracks().add(track2_1_2);
		album2_2.getTracks().add(track2_2_1);
				
		session.save(track1_1_1);
		session.save(track1_1_2);
		session.save(track1_1_3);
		session.save(track2_1_1);
		session.save(track2_1_2);
		session.save(track2_2_1);
		
		session.getTransaction().commit();
		
		HibernateUtil.shutdown();
		System.out.println("Done!");
	} 
}
