package discography.client;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.junit.Before;
import org.junit.Test;

import discography.model.json.ArtistJson;

public class ArtistServiceTest {

	private Client client;
	private String baseUrl = "http://localhost:8080/discography/artist";

	@Before
	public void setUp() throws Exception {
		client = ClientBuilder.newClient();
	}

	@Test
	public void testServices() {

		// insert test data
		ArtistJson artist = new ArtistJson();
		String artistName = "TestArtist1";
		artist.setName(artistName);
		// call create artist service
		client.target(baseUrl).request(MediaType.TEXT_PLAIN)
				.post(Entity.entity(artist, MediaType.APPLICATION_JSON), String.class);

		// call get all artist service
		GenericType<List<ArtistJson>> list = new GenericType<List<ArtistJson>>() {
		};
		List<ArtistJson> artists = client.target(baseUrl).request(MediaType.APPLICATION_JSON).get(list);

		// verify inserted artist in list
		assertFalse(artists.isEmpty());
		Set<String> names = new HashSet<String>();
		for (ArtistJson a : artists) {
			names.add(a.getName());
		}
		assertTrue(names.contains(artistName));

		// remove test data
		Long artistId = 0L;
		for (ArtistJson a : artists) {
			if (artistName.equals(a.getName())) {
				artistId = a.getId();
			}
		}

		// call delete artist service
		client.target(baseUrl).path("{artistId}").resolveTemplate("artistId", artistId).request(MediaType.TEXT_PLAIN)
				.delete(String.class);

		// verify artist deleted
		// call get all artist service
		GenericType<List<ArtistJson>> listAfterDelete = new GenericType<List<ArtistJson>>() {
		};
		List<ArtistJson> artistsAfterDelete = client.target(baseUrl).request(MediaType.APPLICATION_JSON)
				.get(listAfterDelete);
		Set<String> namesAfterDelete = new HashSet<String>();
		for (ArtistJson a : artistsAfterDelete) {
			namesAfterDelete.add(a.getName());
		}
		assertFalse(namesAfterDelete.contains(artistName));

	}

}
