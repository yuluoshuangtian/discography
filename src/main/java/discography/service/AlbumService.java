package discography.service;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import discography.dao.AlbumDao;
import discography.dao.TrackDao;
import discography.model.Album;
import discography.model.Track;
import discography.model.json.AlbumJson;
import discography.model.json.TrackJson;

@Path("album")
public class AlbumService {

	/**
	 * @return Returns the list of all the albums.
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<AlbumJson> getAllAlbums() {
		List<AlbumJson> albumsJson = new ArrayList<AlbumJson>();
		List<Album> albums = AlbumDao.INSTANCE.getAllAlbums();
		for (Album album : albums) {
			AlbumJson albumJson = new AlbumJson();
			albumJson.setId(album.getId());
			albumJson.setName(album.getName());
			albumJson.setYear(album.getYear());
			albumJson.setArtistName(album.getArtist().getName());
			albumsJson.add(albumJson);
		}
		return albumsJson;
	}

	/**
	 * @param albumJson
	 *            {"name":stringValue, "year":intValue,
	 *            "artistName":stringValue}.
	 * @return Create an album and insert it into the database, by the given
	 *         Json data
	 */
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
	public String createAlbum(AlbumJson albumJson) {
		if (AlbumDao.INSTANCE.createAlbum(albumJson.getName(), albumJson.getYear(), albumJson.getArtistName())) {
			return "success";
		} else {
			return "failed";
		}
	}

	/**
	 * @param albumId
	 * @return Returns the album with albumId.
	 */
	@GET
	@Path("{albumId}")
	@Produces(MediaType.APPLICATION_JSON)
	public AlbumJson getAlbum(@PathParam("albumId") String albumId) {
		Album album = AlbumDao.INSTANCE.getAlbumById(Long.parseLong(albumId));
		AlbumJson albumJson = new AlbumJson();
		if (album != null) {
			albumJson.setId(album.getId());
			albumJson.setName(album.getName());
			albumJson.setYear(album.getYear());
			albumJson.setArtistName(album.getArtist().getName());
		}
		return albumJson;
	}

	/**
	 * @param albumId
	 * @return Delete the album with the albumId. Returns the string that shows
	 *         number of album deleted.
	 */
	@DELETE
	@Path("{albumId}")
	@Produces(MediaType.TEXT_PLAIN)
	public String deleteAlbum(@PathParam("albumId") String albumId) {
		int result = AlbumDao.INSTANCE.deleteAlbum(Long.parseLong(albumId));
		return "Number of album deleted: " + result;
	}

	/**
	 * @param albumId
	 * @param albumJson
	 *            {"name":StringValue, "year":intValue,
	 *            "artistName":stringValue}
	 * @return Update the album's name and year (artistName will not be
	 *         updated), by the given Json data. Returns the string that shows
	 *         the number of album updated.
	 */
	@PUT
	@Path("{albumId}")
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
	public String updateAlbum(@PathParam("albumId") String albumId, AlbumJson albumJson) {
		int result = AlbumDao.INSTANCE.updateAlbum(Long.parseLong(albumId), albumJson.getName(), albumJson.getYear());
		return "Number of artist updated: " + result;
	}

	/**
	 * @param albumId
	 * @return Returns the list of tracks that belongs to the album with
	 *         albumId.
	 */
	@GET
	@Path("{albumId}/track")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TrackJson> getTracks(@PathParam("albumId") String albumId) {
		List<TrackJson> tracksJson = new ArrayList<TrackJson>();
		List<Track> tracks = AlbumDao.INSTANCE.getTracksByAlbumId(Long.parseLong(albumId));
		for (Track track : tracks) {
			TrackJson trackJson = new TrackJson();
			trackJson.setId(track.getId());
			trackJson.setName(track.getName());
			trackJson.setAlbumName(track.getAlbum().getName());
			trackJson.setArtistName(track.getArtist().getName());
			tracksJson.add(trackJson);
		}
		return tracksJson;
	}

	/**
	 * @param albumId
	 * @param trackId
	 * @return Returns the track with trackId that belongs to the album with
	 *         albumId.
	 */
	@GET
	@Path("{albumId}/track/{trackId}")
	@Produces(MediaType.APPLICATION_JSON)
	public TrackJson getTrack(@PathParam("albumId") String albumId, @PathParam("trackId") String trackId) {
		TrackJson trackJson = new TrackJson();
		if (AlbumDao.INSTANCE.isTrackBelongsToAlbum(Long.parseLong(albumId), Long.parseLong(trackId))) {
			Track track = TrackDao.INSTANCE.getTrackById(Long.parseLong(trackId));
			if (track != null) {
				trackJson.setAlbumName(track.getAlbum().getName());
				trackJson.setArtistName(track.getArtist().getName());
				trackJson.setName(track.getName());
				trackJson.setId(track.getId());
			}
		}
		return trackJson;
	}
}
