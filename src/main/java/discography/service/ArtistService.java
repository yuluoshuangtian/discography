package discography.service;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import discography.dao.AlbumDao;
import discography.dao.ArtistDao;
import discography.dao.TrackDao;
import discography.model.Album;
import discography.model.Artist;
import discography.model.Track;
import discography.model.json.AlbumJson;
import discography.model.json.ArtistJson;
import discography.model.json.TrackJson;

@Path("artist")
public class ArtistService {

	/**
	 * @return Returns the list of all the artists.
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<ArtistJson> getAllArtists() {
		List<ArtistJson> artistsJson = new ArrayList<ArtistJson>();
		List<Artist> artists = ArtistDao.INSTANCE.getAllArtists();
		for (Artist artist : artists) {
			ArtistJson artistJson = new ArtistJson();
			artistJson.setId(artist.getId());
			artistJson.setName(artist.getName());
			artistsJson.add(artistJson);
		}
		return artistsJson;
	}

	/**
	 * @param artistJson {"name":StringValue}
	 * @return Create an artist and insert it into the database, by the given
	 *         Json data.
	 */
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
	public String createArtist(ArtistJson artistJson) {
		if (ArtistDao.INSTANCE.createArtist(artistJson.getName())) {
			return "success";
		} else {
			return "failed";
		}
	}

	/**
	 * @param artistId
	 * @return Returns the artist with artistId.
	 */
	@GET
	@Path("{artistId}")
	@Produces(MediaType.APPLICATION_JSON)
	public ArtistJson getArtist(@PathParam("artistId") String artistId) {
		Artist artist = ArtistDao.INSTANCE.getArtistById(Long.parseLong(artistId));
		ArtistJson artistJson = new ArtistJson();
		if (artist != null) {
			artistJson.setId(artist.getId());
			artistJson.setName(artist.getName());
		}
		return artistJson;
	}

	/**
	 * @param artistId
	 * @return Delete the artist with the artistId. Returns the string that
	 *         shows number of artist deleted.
	 */
	@DELETE
	@Path("{artistId}")
	@Produces(MediaType.TEXT_PLAIN)
	public String deleteArtist(@PathParam("artistId") String artistId) {
		int result = ArtistDao.INSTANCE.deleteArtist(Long.parseLong(artistId));
		return "Number of artist deleted: " + result;
	}

	/**
	 * @param artistId
	 * @param artistJson
	 *            {"name":StringValue}
	 * @return Update the artist's name, by the given Json data. Returns the
	 *         string that shows the number of artist updated.
	 */
	@PUT
	@Path("{artistId}")
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
	public String updateArtist(@PathParam("artistId") String artistId, ArtistJson artistJson) {
		int result = ArtistDao.INSTANCE.updateArtist(Long.parseLong(artistId), artistJson.getName());
		return "Number of artist updated: " + result;
	}

	/**
	 * @param artistId
	 * @return Returns the list of albums that belongs to the artist with
	 *         artistId
	 */
	@GET
	@Path("{artistId}/album")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AlbumJson> getAlbums(@PathParam("artistId") String artistId) {
		List<Album> albums = ArtistDao.INSTANCE.getAlbumsByArtistId(Long.parseLong(artistId));
		List<AlbumJson> albumsJson = new ArrayList<AlbumJson>();
		for (Album album : albums) {
			AlbumJson albumJson = new AlbumJson();
			albumJson.setId(album.getId());
			albumJson.setName(album.getName());
			albumJson.setYear(album.getYear());
			albumJson.setArtistName(album.getArtist().getName());
			albumsJson.add(albumJson);
		}
		return albumsJson;
	}

	/**
	 * @param artistId
	 * @param albumId
	 * @return Returns the album with albumId that belongs to the artist with
	 *         artistId.
	 */
	@GET
	@Path("{artistId}/album/{albumId}")
	@Produces(MediaType.APPLICATION_JSON)
	public AlbumJson getAlbum(@PathParam("artistId") String artistId, @PathParam("albumId") String albumId) {
		AlbumJson albumJson = new AlbumJson();
		if (ArtistDao.INSTANCE.isAlbumBelongsToArtist(Long.parseLong(artistId), Long.parseLong(albumId))) {
			Album album = AlbumDao.INSTANCE.getAlbumById(Long.parseLong(albumId));
			if (album != null) {
				albumJson.setId(album.getId());
				albumJson.setName(album.getName());
				albumJson.setYear(album.getYear());
				albumJson.setArtistName(album.getArtist().getName());
			}
		}
		return albumJson;
	}

	/**
	 * @param artistId
	 * @param albumId
	 * @return Returns the list of tracks in the album with albumId, that
	 *         belongs to the artist with artistId
	 */
	@GET
	@Path("{artistId}/album/{albumId}/track")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TrackJson> getTracks(@PathParam("artistId") String artistId, @PathParam("albumId") String albumId) {
		List<TrackJson> tracksJson = new ArrayList<TrackJson>();
		if (ArtistDao.INSTANCE.isAlbumBelongsToArtist(Long.parseLong(artistId), Long.parseLong(albumId))) {
			List<Track> tracks = AlbumDao.INSTANCE.getTracksByAlbumId(Long.parseLong(albumId));
			for (Track track : tracks) {
				TrackJson trackJson = new TrackJson();
				trackJson.setId(track.getId());
				trackJson.setName(track.getName());
				trackJson.setAlbumName(track.getAlbum().getName());
				trackJson.setArtistName(track.getArtist().getName());
				tracksJson.add(trackJson);
			}
		}
		return tracksJson;
	}

	/**
	 * @param artistId
	 * @param albumId
	 * @param trackId
	 * @return Returns the track with trackId in the album with albumId, that
	 *         belongs to the artist with artistId.
	 */
	@GET
	@Path("{artistId}/album/{albumId}/track/{trackId}")
	@Produces(MediaType.APPLICATION_JSON)
	public TrackJson getTrack(@PathParam("artistId") String artistId, @PathParam("albumId") String albumId,
			@PathParam("trackId") String trackId) {
		TrackJson trackJson = new TrackJson();
		if (ArtistDao.INSTANCE.isTrackBelongsToArtistAndAlbum(Long.parseLong(artistId), Long.parseLong(albumId),
				Long.parseLong(trackId))) {
			Track track = TrackDao.INSTANCE.getTrackById(Long.parseLong(trackId));
			if (track != null) {
				trackJson.setId(track.getId());
				trackJson.setName(track.getName());
				trackJson.setAlbumName(track.getAlbum().getName());
				trackJson.setArtistName(track.getArtist().getName());
			}
		}
		return trackJson;
	}
}
