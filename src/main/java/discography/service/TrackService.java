package discography.service;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import discography.dao.TrackDao;
import discography.model.Track;
import discography.model.json.TrackJson;

@Path("track")
public class TrackService {

	/**
	 * @return Returns the list of all the tracks.
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<TrackJson> getAllTracks() {
		List<TrackJson> tracksJson = new ArrayList<TrackJson>();
		List<Track> tracks = TrackDao.INSTANCE.getAllTracks();
		if (tracks != null) {
			for (Track track : tracks) {
				TrackJson trackJson = new TrackJson();
				trackJson.setId(track.getId());
				trackJson.setName(track.getName());
				trackJson.setAlbumName(track.getAlbum().getName());
				trackJson.setArtistName(track.getArtist().getName());
				tracksJson.add(trackJson);
			}
		}
		return tracksJson;
	}

	/**
	 * @param trackJson
	 *            {"name":stringValue, "artistName":stringValue,
	 *            "artistName":stringValue}
	 * @return Create a track and insert it into the database, by the given Json
	 *         data.
	 */
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
	public String createTrack(TrackJson trackJson) {
		if (TrackDao.INSTANCE.createTrack(trackJson.getName(), trackJson.getArtistName(), trackJson.getAlbumName())) {
			return "success";
		} else {
			return "failed";
		}
	}

	/**
	 * @param trackId
	 * @return Returns the track with trackId.
	 */
	@GET
	@Path("{trackId}")
	@Produces(MediaType.APPLICATION_JSON)
	public TrackJson getTrack(@PathParam("trackId") String trackId) {
		Track track = TrackDao.INSTANCE.getTrackById(Long.parseLong(trackId));
		TrackJson trackJson = new TrackJson();
		if (track != null) {
			trackJson.setId(track.getId());
			trackJson.setName(track.getName());
			trackJson.setAlbumName(track.getAlbum().getName());
			trackJson.setArtistName(track.getArtist().getName());
		}
		return trackJson;
	}

	/**
	 * @param trackId
	 * @return Delete the track with the trackId. Returns the string that shows
	 *         number of track deleted.
	 */
	@DELETE
	@Path("{trackId}")
	@Produces(MediaType.TEXT_PLAIN)
	public String deleteTrack(@PathParam("trackId") String trackId) {
		int result = TrackDao.INSTANCE.deleteTrack(Long.parseLong(trackId));
		return "Number of track deleted: " + result;
	}

	/**
	 * @param trackId
	 * @param trackJson
	 *            {"name":stringValue, "artistName":stringValue,
	 *            "albumName":stringValue}
	 * @return Update the track's name (artistName and albumName will not be
	 *         updated), by the given Json data. Returns the string that shows
	 *         the number of album updated.
	 */
	@PUT
	@Path("{trackId}")
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
	public String updateTrack(@PathParam("trackId") String trackId, TrackJson trackJson) {
		int result = TrackDao.INSTANCE.updateTrack(Long.parseLong(trackId), trackJson.getName());
		return "Number of track updated: " + result;
	}
}
