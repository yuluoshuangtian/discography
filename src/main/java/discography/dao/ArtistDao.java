package discography.dao;

import java.util.Collections;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import discography.model.Album;
import discography.model.Artist;
import discography.util.HibernateUtil;

public enum ArtistDao {
	INSTANCE;

	@SuppressWarnings("unchecked")
	public List<Artist> getAllArtists() {
		List<Artist> artists = Collections.EMPTY_LIST;
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			artists = session.createQuery("from Artist").list();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return artists;
	}

	public Artist getArtistById(Long id) {
		Artist artist = new Artist();
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("from Artist where id=:id");
			query.setParameter("id", id);
			artist = (Artist) query.uniqueResult();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}

		return artist;
	}

	@SuppressWarnings("unchecked")
	public List<Album> getAlbumsByArtistId(Long artistId) {
		List<Album> albums = Collections.EMPTY_LIST;
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("from Album where artist.id=:artistId");
			query.setParameter("artistId", artistId);
			albums = query.list();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return albums;
	}

	public boolean isAlbumBelongsToArtist(Long artistId, Long albumId) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		boolean isAlbumBelongsToArtist = false;
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("from Album where artist.id=:artistId and id=:albumId");
			query.setParameter("artistId", artistId);
			query.setParameter("albumId", albumId);
			Album album = (Album) query.uniqueResult();
			if (album != null) {
				isAlbumBelongsToArtist = true;
			}
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return isAlbumBelongsToArtist;
	}

	public boolean isTrackBelongsToArtistAndAlbum(Long artistId, Long albumId, Long trackId) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		boolean isTrackBelongsToArtistAndAlbum = false;
		try {
			transaction = session.beginTransaction();
			Query query = session
					.createQuery("from Track where artist.id=:artistId and album.id=:albumId and id=:trackId");
			query.setParameter("artistId", artistId);
			query.setParameter("albumId", albumId);
			query.setParameter("trackId", trackId);
			Album album = (Album) query.uniqueResult();
			if (album != null) {
				isTrackBelongsToArtistAndAlbum = true;
			}
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return isTrackBelongsToArtistAndAlbum;
	}

	public boolean createArtist(String name) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Artist artistToBeCreated = new Artist();
		artistToBeCreated.setName(name);
		boolean isArtistCreationSucceed = false;
		try {
			transaction = session.beginTransaction();
			
			// check if the artist name to be created already exists, if yes, return false
			Query getArtistQuery = session.createQuery("from Artist where name=:name");
			getArtistQuery.setParameter("name", name);
			Artist artist = (Artist)getArtistQuery.uniqueResult();
			if (artist != null){
				return isArtistCreationSucceed;
			}
			
			// create artist
			session.save(artistToBeCreated);
			
			// check if the artist creation succeed
			Query checkQuery = session.createQuery("from Artist where name = :name");
			checkQuery.setParameter("name", name);
			Artist result = (Artist) checkQuery.uniqueResult();
			if (result != null){
				isArtistCreationSucceed = true;
			}
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return isArtistCreationSucceed;
	}

	public int deleteArtist(Long id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		int result = 0;
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("delete from Artist where id=:id");
			query.setParameter("id", id);
			result = query.executeUpdate();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return result;
	}

	public int updateArtist(Long id, String name) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		int result = 0;
		try {
			transaction = session.beginTransaction();
			
			// check if the artist name to be updated already exists, if yes, return 0
			Query getArtistQuery = session.createQuery("from Artist where name=:name");
			getArtistQuery.setParameter("name", name);
			Artist artist = (Artist)getArtistQuery.uniqueResult();
			if (artist != null){
				return result;
			}
			
			// update artist's name
			Query query = session.createQuery("update Artist set name = :name where id = :id");
			query.setParameter("name", name);
			query.setParameter("id", id);
			result = query.executeUpdate();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return result;
	}
}
