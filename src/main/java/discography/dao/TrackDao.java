package discography.dao;

import java.util.Collections;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import discography.model.Album;
import discography.model.Artist;
import discography.model.Track;
import discography.util.HibernateUtil;

public enum TrackDao {
	INSTANCE;

	@SuppressWarnings("unchecked")
	public List<Track> getAllTracks() {
		List<Track> tracks = Collections.EMPTY_LIST;
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			tracks = session.createQuery("FROM Track").list();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return tracks;
	}

	public Track getTrackById(Long id) {
		Track track = new Track();
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("from Track where id=:id");
			query.setParameter("id", id);
			track = (Track) query.uniqueResult();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return track;
	}

	public int deleteTrack(Long id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		int result = 0;
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("delete from Track where id=:id");
			query.setParameter("id", id);
			result = query.executeUpdate();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return result;
	}

	public int updateTrack(Long id, String name) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		int result = 0;
		try {
			transaction = session.beginTransaction();

			// get the track's albumId
			Query getTrackQuery = session.createQuery("from Track where id=:id");
			getTrackQuery.setParameter("id", id);
			Track track = (Track) getTrackQuery.uniqueResult();
			Long albumId = track.getAlbum().getId();

			// check if the album with albumId already has the a track name that
			// to be updated, if yes, return 0
			Query getTrackToBeUpdatedQuery = session.createQuery("from Track where name=:name and album.id=:albumId");
			getTrackToBeUpdatedQuery.setParameter("name", name);
			getTrackToBeUpdatedQuery.setParameter("albumId", albumId);
			Track trackToBeUpdated = (Track) getTrackToBeUpdatedQuery.uniqueResult();
			if (trackToBeUpdated != null) {
				return result;
			}

			// update the track
			Query updateQuery = session.createQuery("update Track set name = :name where id = :id");
			updateQuery.setParameter("name", name);
			updateQuery.setParameter("id", id);
			result = updateQuery.executeUpdate();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return result;
	}

	public boolean createTrack(String name, String artistName, String albumName) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Track track = new Track();
		track.setName(name);
		boolean isTrackCreationSucceed = false;
		try {
			transaction = session.beginTransaction();

			// check if the artist with artistName exists, if not, return false
			Query getArtistQuery = session.createQuery("from Artist where name=:name");
			getArtistQuery.setParameter("name", artistName);
			Artist artist = (Artist) getArtistQuery.uniqueResult();
			if (artist == null) {
				return isTrackCreationSucceed;
			}

			// check if the album with albumName exists, if not return false
			Query getAlbumQuery = session.createQuery("from Album where name=:name");
			getAlbumQuery.setParameter("name", albumName);
			Album album = (Album) getAlbumQuery.uniqueResult();
			if (album == null) {
				return isTrackCreationSucceed;
			}

			// check if the album with albumId belongs to artist with artistId,
			// if not, return false
			Query getAlbumByArtistNameQuery = session
					.createQuery("from Album where name=:name and artist.name=:artistName");
			getAlbumByArtistNameQuery.setParameter("name", albumName);
			getAlbumByArtistNameQuery.setParameter("artistName", artistName);
			Album albumWithAlbumNameAndArtistName = (Album) getAlbumByArtistNameQuery.uniqueResult();
			if (albumWithAlbumNameAndArtistName == null) {
				return isTrackCreationSucceed;
			}

			// check if the album with albumName already has a track name that
			// to be created, if yes, return false
			Query getTrackQuery = session.createQuery("from Track where name=:name and album.name=:albumName");
			getTrackQuery.setParameter("name", name);
			getTrackQuery.setParameter("albumName", albumName);
			Track trackToBeCreated = (Track) getTrackQuery.uniqueResult();
			if (trackToBeCreated != null) {
				return isTrackCreationSucceed;
			}

			// create track
			track.setAlbum(album);
			track.setArtist(artist);
			album.getTracks().add(track);
			artist.getTracks().add(track);
			session.save(track);

			// check if track creation succeed
			Query checkQuery = session
					.createQuery("from Track where name=:name and artist.name=:artistName and album.name=:albumName");
			checkQuery.setParameter("name", name);
			checkQuery.setParameter("artistName", artistName);
			checkQuery.setParameter("albumName", albumName);
			Album result = (Album) checkQuery.uniqueResult();
			if (result != null) {
				isTrackCreationSucceed = true;
			}
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return isTrackCreationSucceed;
	}
}
