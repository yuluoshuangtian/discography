package discography.dao;

import java.util.Collections;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import discography.model.Album;
import discography.model.Artist;
import discography.model.Track;
import discography.util.HibernateUtil;

public enum AlbumDao {
	INSTANCE;

	@SuppressWarnings("unchecked")
	public List<Album> getAllAlbums() {
		List<Album> albums = Collections.EMPTY_LIST;
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			albums = session.createQuery("from Album").list();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return albums;
	}

	public Album getAlbumById(Long id) {
		Album album = new Album();
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("from Album where id=:id");
			query.setParameter("id", id);
			album = (Album) query.uniqueResult();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return album;
	}

	@SuppressWarnings("unchecked")
	public List<Track> getTracksByAlbumId(Long albumId) {
		List<Track> tracks = Collections.EMPTY_LIST;
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("from Track where album.id=:albumId");
			query.setParameter("albumId", albumId);
			tracks = query.list();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return tracks;
	}
	
	public boolean isTrackBelongsToAlbum(Long albumId, Long trackId) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		boolean isTrackBelongsToAlbum = false;
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("from Track where album.id=:albumId and id=:trackId");
			query.setParameter("albumId", albumId);
			query.setParameter("trackId", trackId);
			Album album = (Album) query.uniqueResult();
			if (album != null) {
				isTrackBelongsToAlbum = true;
			}
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return isTrackBelongsToAlbum;
	}

	public int deleteAlbum(Long id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		int result = 0;
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("delete from Album where id=:id");
			query.setParameter("id", id);
			result = query.executeUpdate();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return result;
	}

	public int updateAlbum(Long id, String name, int year) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		int result = 0;
		try {
			transaction = session.beginTransaction();
			
			// get the album's artistId
			Query getAlbumQuery = session.createQuery("from Album where id=:id");
			getAlbumQuery.setParameter("id", id);
			Album album = (Album) getAlbumQuery.uniqueResult();
			Long artistId = album.getArtist().getId();			
			
			// check if the artist with artistId already has an album name that to be updated, if yes, return 0
			Query getAlbumToBeUpdatedQuery = session.createQuery("from Album where name=:name and artist.id=:artistId");
			getAlbumToBeUpdatedQuery.setParameter("name", name);
			getAlbumToBeUpdatedQuery.setParameter("artistId", artistId);
			Album albumToBeUpdated  = (Album)getAlbumToBeUpdatedQuery.uniqueResult();
			if (albumToBeUpdated != null){
				return result;
			}
			
			// update the album's year and name
			Query updateQuery = session.createQuery("update Album set name = :name, year = :year where id = :id");
			updateQuery.setParameter("name", name);
			updateQuery.setParameter("year", year);
			updateQuery.setParameter("id", id);
			
			result = updateQuery.executeUpdate();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return result;
	}

	public boolean createAlbum(String name, int year, String artistName) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Album albumToBeCreated = new Album();
		albumToBeCreated.setName(name);
		albumToBeCreated.setYear(year);
		boolean isAlbumCreationSucceed = false;
		try {
			transaction = session.beginTransaction();
			
			// check if the artist with artistName exists, if not, return false
			Query getArtistQuery = session.createQuery("from Artist where name=:name");
			getArtistQuery.setParameter("name", artistName);
			Artist artist = (Artist) getArtistQuery.uniqueResult();
			if (artist == null){
				return isAlbumCreationSucceed;
			}
			
			// check if the artist with artistName already has the album name to be created, if yes, return false
			Query getAlbumQuery = session.createQuery("from Album where name=:name and artist.name=:artistName");
			getAlbumQuery.setParameter("name", name);
			getAlbumQuery.setParameter("artistName", artistName);
			Album album = (Album)getAlbumQuery.uniqueResult();
			if (album != null){
				return isAlbumCreationSucceed;
			}
			
			// create album
			albumToBeCreated.setArtist(artist);
			artist.getAlbums().add(albumToBeCreated);
			session.save(albumToBeCreated);
			
			// check if the creation succeeded
			Query checkQuery = session.createQuery("from Album where name=:name and artist.name=:artistName");
			checkQuery.setParameter("name", name);
			checkQuery.setParameter("artistName", artistName);
			Album result = (Album)checkQuery.uniqueResult();
			if (result != null){
				isAlbumCreationSucceed = true;
			}
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return isAlbumCreationSucceed;
	}
}