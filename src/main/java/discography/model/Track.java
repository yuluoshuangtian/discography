package discography.model;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "TRACK")
public class Track implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private String name;
	private Album album;
	private Artist artist;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "TRACK_ID", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "TRACK_NAME", unique = true, nullable = false)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ManyToOne
	@JoinColumn(name = "ALBUM_ID", nullable = false)
	public Album getAlbum() {
		return this.album;
	}

	public void setAlbum(Album album) {
		this.album = album;
	}

	@ManyToOne
	@JoinColumn(name = "ARTIST_ID", nullable = false)
	public Artist getArtist() {
		return this.artist;
	}

	public void setArtist(Artist artist) {
		this.artist = artist;
	}
}
